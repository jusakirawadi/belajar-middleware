<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uri = $request->path();
        $role = Auth::user()->role_id;
        
        // jika uri yang dibuka route-1, 
        // maka hanya diakses jika role = 2 ( super admin )
        if ($uri == 'route-1') {
            if ($role == 2) {
                return $next($request);        
            }
            abort(403);
        }

        // jika uri yang dibuka route-2, 
        // maka hanya diakses jika role = 1, 2 ( <> 0 , bukan guest)
        if ($uri == 'route-2') {
            if ($role <> 0) {
                return $next($request);        
            }
            abort(403);
        }
        
        // jika uri yang dibuka route-3, 
        // maka dapat diakses semua user
        if ($uri == 'route-3') {
            return $next($request);        
        }

        // di luar URI di atas, blok saja aksesnya
        abort(403);
    }
}

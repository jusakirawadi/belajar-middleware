<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function route1 () {
        return view('superadmin');
    }
    
    public function route2 () {
        return view('admin');
    }

    public function route3 () {
        return view('guest');
    }
}
